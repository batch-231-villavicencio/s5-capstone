class Product {
	constructor(name,price){
		this.name = name;
		this.price = price;
        this.isActive = true;
	}
    archive(){
        if(this.isActive == true){
            this.isActive = false;
        }
        return this
    }
	updatePrice(newPrice){
        this.price = newPrice;
        return this
    }
}

class Cart {
    constructor(){
        this.contents = [];
        this.totalAmount = undefined;
    }

    addToCart(product,quantity){
        
        this.contents.push({
            product:product,
            quantity:quantity
        })
        this.computeTotal()
        return this
    }
    showCartContents(){
        console.log(this.contents)
        return this
    }
    updateProductQuantity(productName,newQuantity){
        this.contents.forEach(product =>{
            if (product.product.name == productName){
                product.quantity = newQuantity;
            }
        })
        this.computeTotal()
        return this
    }
    clearCartContents(){
        this.contents = [];
        this.totalAmount = 0;
        return this
    }
    computeTotal(){
        let sum = 0
        this.contents.forEach(product =>{
            sum = sum + (product.product.price * product.quantity)
        })
        this.totalAmount = sum;
        return this
    }
}

class Customer {
    constructor(email){
        this.email = email
        this.cart = (new Cart())
        this.orders = []
    }
    checkOut(){
        this.cart.computeTotal()
        this.orders.push({
            products: this.cart.contents,
            totalAmount: this.cart.totalAmount
        })
        
        return this
    }
}

const john = new Customer('john@mail.com')

const prodA = new Product('soap', 9.99)
const prodB = new Product('shampoo', 12.99)
const prodC = new Product('toothbrush', 4.99)
const prodD = new Product('toothpaste', 14.99)




